/* Here goes your JS code */

document.getElementById("show-popup-form").addEventListener("click", function () {
    document.querySelector('.popup').style.display = "flex";
});

function comeBack() {
    document.querySelector(".popup").style.display = "none";
}

function validate() {

    let pass = document.myForm.password.value;
    let passLength = pass.length;
    let isThereCapitalLetter = false;
    let isThereLowerLetter = false;

    for (let i = 0; i < passLength; i++) {
        if (pass.charAt(i) === pass.charAt(i).toUpperCase()) {
            isThereCapitalLetter = true;
        }
    }

    for (let i = 0; i < passLength; i++) {
        if (pass.charAt(i) === pass.charAt(i).toLowerCase()) {
            isThereLowerLetter = true;
        }
    }

    if (passLength >= 7 && isThereCapitalLetter && isThereLowerLetter) {

        setTimeout(function () {

            document.querySelector(".popup").style.display = "none";
            document.getElementById("show-goodbye-message").style.display = "flex";
            document.getElementById("show-popup-form").style.display = "none";

        }, 3000);

        event.preventDefault();
    }
}
